{
  description = "Advent of Code 2021 solutions.";

  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixos-unstable";
    flake-utils.url = "github:numtide/flake-utils";
    rust-overlay.url = "github:oxalica/rust-overlay";
  };

  outputs = { nixpkgs, flake-utils, rust-overlay, ... }:
    flake-utils.lib.eachDefaultSystem (system:
      let
        overlays = [
          (import rust-overlay)
        ];
        pkgs = import nixpkgs {
          inherit system overlays;
        };
        rustToolchain = pkgs.rust-bin.fromRustupToolchainFile ./rust-toolchain.toml;
      in
      {
        devShell = pkgs.mkShell {
          nativeBuildInputs = [
            rustToolchain
            pkgs.clang
            pkgs.nixpkgs-fmt
            pkgs.openssl
            pkgs.pkgconfig
          ];

          PKG_CONFIG_PATH = "${pkgs.pkgconfig}/lib/pkgconfig";
        };
      }
    );
}
