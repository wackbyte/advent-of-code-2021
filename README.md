# Advent of Code 2021

My solutions for [Advent of Code 2021](https://adventofcode.com/2021).

You can run this project with the help of [`cargo-aoc`](https://github.com/gobanos/cargo-aoc).
