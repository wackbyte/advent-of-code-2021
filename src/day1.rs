#[aoc_generator(day1)]
fn input_generator(input: &str) -> Vec<u16> {
    input
        .lines()
        .map(|line| line.parse::<u16>().unwrap())
        .collect::<Vec<u16>>()
}

#[aoc(day1, part1)]
fn solve_part1(input: &[u16]) -> usize {
    input
        .windows(2)
        .filter(|window| window[1] > window[0])
        .count()
}

#[aoc(day1, part2)]
fn solve_part2(input: &[u16]) -> usize {
    solve_part1(
        &input
            .windows(3)
            .map(|window| window.into_iter().sum())
            .collect::<Vec<u16>>(),
    )
}
