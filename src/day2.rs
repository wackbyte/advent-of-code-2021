#[derive(Copy, Clone, Debug, Eq, PartialEq)]
enum Direction {
    Forward,
    Down,
    Up,
}

#[derive(Copy, Clone, Debug, Eq, PartialEq)]
struct Instruction {
    direction: Direction,
    amount: u8,
}

#[aoc_generator(day2)]
fn input_generator(input: &str) -> Vec<Instruction> {
    input
        .lines()
        .map(|line| {
            let (direction, amount) = line.split_once(' ').unwrap();
            Instruction {
                direction: match direction {
                    "forward" => Direction::Forward,
                    "down" => Direction::Down,
                    "up" => Direction::Up,
                    _ => unreachable!(),
                },
                amount: amount.parse::<u8>().unwrap(),
            }
        })
        .collect::<Vec<Instruction>>()
}

#[aoc(day2, part1)]
fn solve_part1(input: &[Instruction]) -> i32 {
    let (distance, depth): (u32, i32) = input.into_iter().fold(
        (0, 0),
        |(distance, depth), &Instruction { direction, amount }| match direction {
            Direction::Forward => (distance + (amount as u32), depth),
            Direction::Down => (distance, depth + (amount as i32)),
            Direction::Up => (distance, depth - (amount as i32)),
        },
    );
    (distance as i32) * depth
}

#[aoc(day2, part2)]
fn solve_part2(input: &[Instruction]) -> i32 {
    let (distance, depth, _): (u32, i32, i32) = input.into_iter().fold(
        (0, 0, 0),
        |(distance, depth, aim), &Instruction { direction, amount }| match direction {
            Direction::Forward => (
                distance + (amount as u32),
                depth + (amount as i32) * aim,
                aim,
            ),
            Direction::Down => (distance, depth, aim + (amount as i32)),
            Direction::Up => (distance, depth, aim - (amount as i32)),
        },
    );
    (distance as i32) * depth
}
