/// get a bit
const fn bit(bits: u16, index: u8) -> u16 {
    (bits >> index) & 1
}

/// clamp a result to the max report value
const fn clamp(bits: u32) -> u32 {
    bits & 0b1111_1111_1111
}

#[aoc_generator(day3)]
fn input_generator(input: &str) -> Vec<u16> {
    input
        .lines()
        .map(|line| u16::from_str_radix(line, 2).unwrap())
        .collect::<Vec<u16>>()
}

#[aoc(day3, part1)]
fn solve_part1(input: &[u16]) -> u32 {
    let total = input.len() as u16;
    let gamma_rate: u32 = (0..12).fold(0, |x, i| {
        let ones: u16 = input.iter().map(|binary| bit(*binary, i)).sum();
        let zeros: u16 = total - ones;
        if ones > zeros {
            x | (1 << i)
        } else {
            x
        }
    });
    let epsilon_rate = clamp(!gamma_rate);
    gamma_rate * epsilon_rate
}
